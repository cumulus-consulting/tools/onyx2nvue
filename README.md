# onyx2nvue

Convert Onyx to NVUE


## Usage

```
python3 convert.py -i <sourcefile> -o <destinationfile>
```

If `-o` isn't provided, file will be automatically written to `output.txt` in local directory.