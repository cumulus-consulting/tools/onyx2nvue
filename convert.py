#!/bin/env python3.7
# Start date 19/07/2021
# Convert Onyx configs into CL configs
# Version 0.1 / Dated 19/07/2021 / bad coding by Michael Schipp
# Version 0.11 / Dated 21/07/2021 / Added PTP
# Version 0.9 / Dated 21/09/2021 / Added all remaining features for basic L2 and L3, two switch Onyx functionality
# Version 0.91 / Dated 23/09/2021 / Added breakout support
# Version 0.92 / Dated 24/09/2021 / Fixing MLAG backup ip bugs
# Version 0.93 / Dated 24/09/2021 / Added VRRP-->VRR support
# Version 0.94 / Dated 22/10/2021 / Refactored BGP and Hostname to align to CL5.0
# Version 1.00 / Dated 12/1/2022 / Fixed ip name-server logic
# Version 1.1 / Dated 14/3/2022 / Fixed CIDR logic
# Version 1.4 / Dated 17/3/2023 / Added support for CL 5.4 - Updated by Krishna Vasudevan
# Version 1.5 / Dated 01/06/2023 / Added support for CL 5.5, TACACS - Updated by Krishna Vasudevan
# Version 1.6 / Dated 10/10/2023 / Bug fixes for HPE - Updated by Krishna Vasudevan
# Version 1.7 / Dated 07/19/2024 / Bug fixes - Updated by Krishna Vasudevan
# Version 1.8 / Dated 08/05/2024 / Bug fixes - Updated by Krishna Vasudevan
# Version 1.9 / Dated 08/28/2024 / Bug fixes - Updated by Krishna Vasudevan
# Version 1.10 / Dated 08/30/2024 / Added support for CL 5.10, prefix lists, route maps, access lists - Updated by Krishna Vasudevan
# Version 1.10.1 / Dated 10/21/2024 / Bug fixes, updated support for traffic pools, DCB, traffic class
# Version 1.10.2 / Dated 11/05/2024 / MAGP support

### COMMENTS
# Any line that is not converted will be addressed with one of the following tags:
#  # FUTURE SUPPORT - Feature coming to NVUE in the future
#  # UNREQUIRED - Command no longer necessary in NVUE
#  # MANUAL REVIEW - Requires manual review of command before entry, it may not have been captured by the script
#  # SCRIPT UNSUPPORTED - Script doesn't handle input, and most likely not a relevant command in NVUE


import sys, getopt, re
from ipaddress import IPv4Interface,IPv4Network

current_ver = '1.10.2'
NVUE = "nv set "
NVUE_UNSET = "nv unset "
inputfile = ''
outputfile = 'output.txt'
DEBUG = False
MLAGCONFIG = False
MLAGPORTCHANNEL='portchannel1'
MLAGVLAN=0
MLAGMEMBERINTERFACE='swp31-32'
BGPPEER=[]

def main(argv):
    if not argv:     #check if no arg are entered
        print ('convert.py -i <inputfile> -o <outputfile> -c')
        print ('')
        print ('Usage:')
        print ('Input file must be specified by using -i')
        print ('If -o is not specified then default of output.txt is used')
        print ('Optional -c will use cl set instead of the default of nv set')
        print ('-d is the DEBUG')
        sys.exit()    # no arg, bugging out of here
    global inputfile,outputfile,NVUE,DEBUG  #use gloabal variable
    try:
        opts, args = getopt.getopt(argv,"hi:o:cd",["ifile=","ofile="])
    except getopt.GetoptError:
        print ('convert.py -i <inputfile> -o <outputfile> -c -d')
        print ('')
        print ('Usage:')
        print ('If -o is not specified then default of output.txt is used')
        print ('-c will use cl set, default is to use nv set')
        print ('-d is the DEBUG')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print ('convert.py -i <inputfile> -o <outputfile> -c')
            print ('')
            print ('Usage:')
            print ('If -o is not specified then default of output.txt is used')
            print ('-c will use cl set, default is to use nv set')
            print ('-d is the DEBUG')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt == '-c':
            NVUE = "cl"
        elif opt == '-d':
            DEBUG = True

def netmask2cidr(netmask):

    cidr = str(sum([str(bin(int(octet))).count("1") for octet in netmask.split(".")]))

    return cidr

if __name__ == "__main__":
   main(sys.argv[1:])

line_output = []
in_data2 = []
line_str = ""
temp_list = []
temp_str = ""
write_line = True
input_file = open(inputfile,'r')            # open the file for raeading only
in_data = input_file.readlines()            # read the file into a list
output_file = open(outputfile,'w')           # open the file for writting to

print("DEBUG: " + str(DEBUG))

title = '#!/bin/bash\n# Converted from Onyx to Cumulus Linux 5.10.0\nset -x\n'
output_file.write(title)

## Initial cleaning of the data before processing it
# first let us change interface format from 1/x to swpx
for line_data in in_data:
    line_str = line_data
    if line_str.find('interface ethernet 1/') != -1:
        line_str = line_str.replace(' 1/', ' swp')

        # Control for interface ranges
        line_str = line_str.replace('-swp', '-')

        # Control for breakouts syntax change
        line_str = line_str.replace('/','s')
        line_str = re.sub('-[0-9]+s','-',line_str)

        if re.search('swp[0-9]+s',line_str) != None:
            line_str = line_str.replace('s1-4','s0-3')

    elif line_str.startswith('no '):
        line_str = '# SCRIPT UNSUPPORTED ' + line_str

    if line_str.find('ipl') != -1:
        MLAGCONFIG = True
        if DEBUG:
            print('MLAG FOUND')
        if line_str.find('port-channel') != -1:
            temp_list = line_str.split()
            MLAGPORTCHANNEL = temp_list[2]
        if line_str.find('vlan') != -1:
            temp_list = line_str.split()
            MLAGVLAN = temp_list[2]

    in_data2.append(line_str)

## Config processing after initial cleanup
for line_data in in_data2:
    if DEBUG:
        print("ORIG:\n" + line_data.rstrip())

    i = 0
    if len(line_data.lstrip()) == 0:
        line_str = '\n'
    else:
        line_str = line_data.lstrip()

    if DEBUG:
        print("STRIPPED:\n" + line_str.rstrip())

    ### Comments
    if line_str.startswith('#'):
        pass
    elif line_str.startswith('\n'):
        pass
    
    else:
        temp_list = line_str.split()
        if DEBUG:
            print("Temp list:")
            print(temp_list)

        if line_str.startswith('show running-config'):
            line_str = "# " + line_str
        ### Protocols
        elif line_str.startswith('protocol'):
            if line_str.find('ptp') != -1 :
                line_str = NVUE +'service ptp 1 enable on\n'
            elif line_str.find('bgp') != -1 :
                line_str = NVUE +'vrf default router bgp enable on\n'
            elif line_str.find('ospf') != -1 :
                line_str = NVUE +'vrf default router ospf enable on\n'
            elif line_str.find('vrrp') != -1 :
                line_str = NVUE + 'system global anycast-id 1\n'
            else:
                line_str = '# UNREQUIRED ' + line_str

        ## Unsupported config - updated in 1.4
        ### Dot1x
        elif line_str.startswith('dot1x') or line_str.find('dot1x') != -1:
            line_str = '# NOT SUPPORTED ' + line_str
        ### Sflow
        elif line_str.startswith('sflow') or line_str.find('sflow') != -1:
            line_str = '# NOT SUPPORTED ' + line_str
        ### Telemetry
        elif line_str.startswith('telemetry') or line_str.find('telemetry') != -1:
            line_str = '# NOT SUPPORTED ' + line_str
        ### Docker
        elif line_str.startswith('docker') or line_str.find('docker') != -1:
            line_str = '# NOT SUPPORTED ' + line_str

        ### Interface - Code cleaned up in version 1.4
        ### MLAG protocol
        elif (line_str.startswith('interface port-channel ' + str(MLAGPORTCHANNEL)) and MLAGCONFIG == True):
            line_str = '# MLAG CONVERSION ' + line_str

        ## Common interface commands
        elif line_str.startswith('interface'):
            # Define interface name depending on type of interface
            if line_str.startswith('interface ethernet'):
                interface_name = temp_list[temp_list.index('ethernet') + 1]
                interface_type = 'swp'
            elif line_str.startswith('interface mlag-port-channel'):
                interface_name = 'bond' + temp_list[temp_list.index('mlag-port-channel') + 1]
                interface_type = 'bond'
            elif line_str.startswith('interface port-channel'):
                interface_name = 'bond' + temp_list[temp_list.index('port-channel') + 1]
                interface_type = 'bond'
            elif line_str.startswith('interface loopback'):
                interface_name = 'lo'
                interface_type = 'loopback'
            elif line_str.startswith('interface vlan'):
                interface_name = 'vlan' + temp_list[temp_list.index('vlan') + 1]
                interface_type = 'svi'
            elif line_str.startswith('interface nve'):
                line_str = "# MANUAL REVIEW " + line_str
            else:
                line_str = "# MANUAL REVIEW " + line_str


            # Link parameters
            if line_str.find('speed') >= 0:
                # Link Lanes e.g.:interface ethernet swp13-14 speed 100GxAuto/100Gx4 - Updated in 1.6
                speed = temp_list[temp_list.index('speed') + 1]
                if speed.find('x') != -1:
                    speed_updated = speed.split('x')[0]
                    line_str = NVUE + 'interface ' + interface_name + ' link speed ' + speed_updated + '\n'
                    if speed.find('Auto') == -1:
                        lanes = speed.split('x')[1]
                        line_str = line_str + NVUE + 'interface ' + interface_name + ' link lanes ' + lanes + '\n'
                else:
                    line_str = NVUE + 'interface ' + interface_name + ' link speed ' + speed + '\n'
            elif line_str.find('mtu') >= 0:
                line_str = NVUE + 'interface ' + interface_name + ' link mtu ' + temp_list[temp_list.index('mtu') + 1] + '\n'
            elif line_str.find('duplex') >= 0:
                line_str = NVUE + 'interface ' + interface_name + ' link duplex ' + temp_list[temp_list.index('duplex') + 1] + '\n'
            elif line_str.find('no shutdown') != -1:
                line_str = NVUE + 'interface ' + interface_name + ' link state up\n'
            elif line_str.find('shutdown') >= 0:
                line_str = NVUE + 'interface ' + interface_name + ' link state down\n'
            # FEC - added in 1.4
            elif line_str.find('fec-override') != -1:
                if temp_list[-1] == 'fc-fec':
                    fec = 'baser'
                elif temp_list[-1] == 'rs-fec':
                    fec = 'rs'
                else:
                    fec = 'off'
                line_str = NVUE + 'interface ' + interface_name + ' link fec ' + fec + '\n'
            # Breakout ports - updated in 1.4 to work with CL 5.4
            # qsfp-split-2 - port is split into 2 ports using QSFP module, each can run at up to 50GbE
            # qsfp-split-4 - port is split into 4 ports using QSFP module, each can run at up to 25GbE
            # qsfp-dd-split-2 - port is split into 2 ports using QSFP-DD module, each can run at up to 200GbE.
            # qsfp-dd-split-4 - port is split into 4 ports using QSFP-DD module, each can run at up to 100GbE.
            # qsfp-dd-split-8 - port is split into 8 ports using QSFP-DD module, each can run at up to 50GbE.
            elif line_str.find('module-type') != -1:
                if line_str.find('qsfp-split-2') >= 0:
                    line_str = NVUE + 'interface ' + interface_name + ' link breakout 2x\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + 's0-1 link state up\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + 's0-1 link speed 50G\n'
                elif line_str.find('qsfp-split-4') >= 0:
                    line_str = NVUE + 'interface ' + interface_name + ' link breakout 4x\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + 's0-3 link state up\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + 's0-3 link speed 25G\n'
                elif line_str.find('qsfp-dd-split-2') >= 0:
                    line_str = NVUE + 'interface ' + interface_name + ' link breakout 2x\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + 's0-1 link state up\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + 's0-1 link speed 200G\n'
                elif line_str.find('qsfp-dd-split-4') >= 0:
                    line_str = NVUE + 'interface ' + interface_name + ' link breakout 4x\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + 's0-3 link state up\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + 's0-3 link speed 100G\n'
                elif line_str.find('qsfp-dd-split-8') >= 0:
                    line_str = NVUE + 'interface ' + interface_name + ' link breakout 8x\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + 's0-7 link state up\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + 's0-7 link speed 50G\n'
        
            # Interface type
            elif len(line_str.split()) == 3 and  not line_str.startswith('interface mlag-port-channel'):
                line_str = NVUE + 'interface ' + interface_name + ' type ' + interface_type + '\n'
            elif len(line_str.split()) == 3 and line_str.startswith('interface mlag-port-channel'):
                line_str = "# MLAG CONVERSION\n"
                line_str = line_str + NVUE + 'interface ' + interface_name + ' type ' + interface_type + '\n'
                # Updated to account for continous subnet IDs mentioned - Eg: interface mlag-port-channel 1-7 - Updated in 1.6
                if temp_list[2].find('-') >=0 :
                    mlag_range = temp_list[2].split('-')
                    for i in range(int(mlag_range[0]),int(mlag_range[1])+1):
                        line_str = line_str + NVUE + 'interface bond' + str(i) + ' bond mlag id ' + str(i) +'\n'    
                else:
                    line_str = line_str + NVUE + 'interface ' + interface_name + ' bond mlag id ' + temp_list[2] +'\n'

            # Description
            elif line_str.find(' description ') != -1:
                line_str = NVUE + 'interface ' + interface_name + ' description "' + " ".join(temp_list[(temp_list.index('description') + 1):]) + '"\n'
            
            # IP Address - added support for mgmt0 (eth0)
            elif line_str.find('ip address') != -1 and line_str.find('loopback0') == -1:
                unset_line_str = ""
                if line_str.find('mgmt0') != -1:
                    interface_name = 'eth0'
                    # Unset the default DHCP configuration - CL 5.9.1
                    unset_line_str = NVUE_UNSET + 'interface eth0 ip address\n'
                # This is a workaround to undo some bad logic in the breakout identification
                if (temp_list[temp_list.index('address') + 1].find('s') != -1):
                    # bug_correction = temp_list[-1].replace('s', '/')
                    # line_str = line_str.replace(' ' + temp_list[-1],' ' + bug_correction)
                    ip_addr = temp_list[temp_list.index('address') + 1].replace('s', '/')
                elif line_str.find('/') == -1:
                    # line_str = line_str.replace(' ' + temp_list[-1],'/' + netmask2cidr(str(temp_list[-1])))
                    ip_addr = temp_list[-2] + '/' + netmask2cidr(str(temp_list[-1]))
                elif line_str.find('/') != -1:
                    if temp_list[-2].find('/') != -1:
                        ip_addr = temp_list[-2]
                    elif temp_list[-1].find('/') != -1:
                        ip_addr = temp_list[-2] + temp_list[-1]
                else:
                    ip_addr = temp_list[temp_list.index('address') + 1]
                line_str = unset_line_str + NVUE + 'interface ' + interface_name + ' ip address ' + ip_addr + '\n'
            elif line_str.find('ip address alias') != -1:
                line_str = "# SCRIPT UNSUPPORTED " + line_str
            elif line_str.find('ip enable') != -1:
                line_str = NVUE + 'interface ' + interface_name + ' ip ipv4 forward on\n'
            # IPv6 - Added in 1.4
            elif line_str.find('ipv6 enable') != -1:
                line_str = NVUE + 'interface ' + interface_name + ' ip ipv6 enable on\n'
            elif line_str.find('ipv6 address') != -1 and line_str.find('autoconfig') == -1:
                line_str = NVUE + 'interface ' + interface_name + ' ip address ' + temp_list[temp_list.index('address') + 1] + '\n'
            
            # PTP
            elif line_str.find('ptp enable') != -1:
                line_str = NVUE + 'interface ' + interface_name + ' ptp enable on \n'
                if line_str.find('forced-master') != -1:
                    line_str = NVUE + 'interface ' + interface_name + ' ptp forced-master on \n'
            # Timers - Added in 1.4
            elif line_str.find('ptp announce') != -1 or line_str.find('ptp delay-req') != -1:
                line_str = NVUE + 'interface ' + interface_name + ' ptp timers ' + temp_list[-3] + '-' + temp_list[-2] + ' ' + temp_list[-1] + '\n'

            # Switchport/Bridge config - updated in 1.4
            elif line_str.find('switchport mode hybrid') != -1 or line_str.find('switchport mode trunk') != -1:
                line_str = NVUE + 'interface ' + interface_name + ' bridge domain br_default\n'
            elif line_str.find('switchport access') != -1:
                line_str = NVUE + 'interface ' + interface_name + ' bridge domain br_default untagged 1\n'
            elif line_str.find('switchport trunk allowed-vlan') != -1 or line_str.find('switchport hybrid allowed-vlan') != -1:
                if line_str.find('none') != -1:
                    line_str = '# UNREQUIRED ' + line_str
                elif line_str.find('add') != -1:
                    line_str = NVUE + 'interface ' + interface_name + ' bridge domain br_default vlan ' + temp_list[-1] + '\n'
                elif line_str.find('switchport trunk allowed-vlan remove') != -1:
                    line_str = NVUE_UNSET + 'interface ' + interface_name + ' bridge domain br_default vlan ' + temp_list[-1] + '\n'
                elif line_str.find('switchport trunk allowed-vlan except') != -1:
                    line_str = NVUE + 'interface ' + interface_name + ' bridge domain br_default vlan all\n'
                    line_str = line_str + NVUE_UNSET + 'interface ' + interface_name + ' bridge domain br_default vlan ' + temp_list[-1] + '\n'
                else:
                    line_str = NVUE + 'interface ' + interface_name + ' bridge domain br_default vlan ' + temp_list[-1] + '\n'
            elif line_str.find('no switchport') >= 0:
                line_str = '# UNREQUIRED ' + line_str

            # LACP - Added in 1.4 - Updated in 1.6 to change interface name to bond names since this is a bond parameter
            elif line_str.find('lacp rate fast') != -1:
                # account for MLAG member interface
                if interface_name != MLAGMEMBERINTERFACE:
                    line_str = '# MANUAL REVIEW (Check bond name) ' + NVUE + 'interface ' + interface_name.replace('swp','bond') + ' bond lacp-rate fast\n'
                else:
                    line_str = '# UNREQUIRED' + line_str
            elif line_str.find('lacp-individual') != -1:
                if interface_name != MLAGMEMBERINTERFACE:
                    line_str = NVUE + 'interface ' + interface_name.replace('swp','bond') + ' bond lacp-bypass on\n'
                else:
                    line_str = '# UNREQUIRED' + line_str

            # STP - Added in 1.4
            elif line_str.find('spanning-tree bpduguard enable') != -1:
                line_str = NVUE + 'interface ' + interface_name + ' bridge domain br_default stp bpdu-guard on\n'
            elif line_str.find('spanning-tree bpdufilter enable') != -1:
                line_str = NVUE + 'interface ' + interface_name + ' bridge domain br_default stp bpdu-filter on\n'
            elif line_str.find('spanning-tree port type') != -1:
                if line_str.find('edge') != -1:
                    line_str = '# Cumulus Linux enables PortAutoEdge by default\n'
                elif line_str.find('network') != -1:
                    line_str = NVUE + 'interface ' + interface_name + ' bridge domain br_default stp network on\n'
                elif line_str.find('bpdufilter') != -1:
                    line_str = NVUE + 'interface ' + interface_name + ' bridge domain br_default stp bpdu-filter on\n'
                elif line_str.find('bpduguard') != -1:
                    line_str = NVUE + 'interface ' + interface_name + ' bridge domain br_default stp bpdu-guard on\n'
        
            # OSPF - Added in 1.4
            elif line_str.find('ip ospf') != -1:
                if line_str.find('cost') != -1:
                    line_str = NVUE + 'interface ' + interface_name + ' router ospf cost ' + temp_list[-1] + '\n'
                elif line_str.find('interval') != -1:
                    line_str = NVUE + 'interface ' + interface_name + ' router ospf timers ' + temp_list[-2] + ' ' + temp_list[-1] + '\n'
                elif line_str.find('priority') != -1:
                    line_str = NVUE + 'interface ' + interface_name + ' router ospf priority ' + temp_list[-1] + '\n'
                elif line_str.find('network') != -1:
                    line_str = NVUE + 'interface ' + interface_name + ' router ospf network-type ' + temp_list[-1] + '\n'
                elif line_str.find('passive-interface') != -1:
                    line_str = NVUE + 'interface ' + interface_name + ' router ospf passive on\n'
                elif line_str.find('shutdown') != -1:
                    line_str = NVUE + 'interface ' + interface_name + ' router ospf enable off\n'
                elif line_str.find('area') != -1:
                    line_str = NVUE + 'interface ' + interface_name + ' router ospf area ' + temp_list[-1] + '\n'
                elif line_str.find('bfd') != -1:
                    line_str = NVUE + 'interface ' + interface_name + ' router ospf bfd enable on\n'

            # ACL - Added in 1.10
            elif line_str.find('port access-group') != -1:
                line_str = NVUE +'interface ' + interface_name + ' acl ' + temp_list[temp_list.index('access-group') + 1] + ' inbound\n'
 
            ## Ethernet Interface
            elif line_str.find('interface ethernet') != -1:
                # MLAG/Bond
                if line_str.find('mlag-channel-group') >= 0:
                    temp_list = line_str.split()
                    line_str = NVUE + 'interface bond' + temp_list[4] + ' bond member ' + interface_name + '\n'
                elif line_str.find('channel-group ' + MLAGPORTCHANNEL) >= 0 and MLAGCONFIG == True:
                    temp_list = line_str.split()
                    MLAGMEMBERINTERFACE = temp_list[2]
                    line_str = '# MLAG CONVERSION ' + line_str
                elif line_str.find('channel-group') >= 0:
                    temp_list = line_str.split()
                    line_str = NVUE + 'interface bond' + temp_list[4] + ' bond member ' + interface_name + '\n'

                # QoS Switch Config - Updated in 1.10.1
                elif line_str.find('qos') != -1:
                    if line_str.find('trust port') != -1:
                        line_str = NVUE + 'qos pfc default-global tx enable\n'
                        line_str = line_str + NVUE + 'qos pfc default-global rx enable\n'
                        line_str = line_str + NVUE + 'qos pfc default-global cable-length 50\n'
                    elif line_str.find('default switch-priority') != -1:
                        line_str = NVUE + 'qos pfc default-global switch-priority' + temp_list[temp_list.index('switch-priority') + 1] + '\n'
                    else:
                        line_str = '# SCRIPT UNSUPPORTED ' + line_str + '\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + ' lldp dcbx-pfc-tlv on\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + ' qos pfc profile default-global\n'

                # DCB - Added in 1.10.1
                elif line_str.find('dcb priority-flow-control mode on') != -1:
                    line_str = NVUE + 'interface ' + interface_name + ' lldp dcbx-pfc-tlv on\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + ' qos pfc profile default-global\n'
                
                # Traffic class - updated in 1.10.1
                elif line_str.find('traffic-class') != -1:
                    if line_str.find('wrr') != -1:
                        line_str = NVUE + 'qos egress-scheduler default-ets traffic-class ' + temp_list[temp_list.index('traffic-class') + 1] + ' mode dwrr\n'
                        line_str = line_str + NVUE + 'qos egress-scheduler default-ets traffic-class ' + temp_list[temp_list.index('traffic-class') + 1] + ' bw-percent ' + temp_list[-1] + '\n'
                    elif line_str.find('strict') != -1:
                        line_str = NVUE + 'qos egress-scheduler default-ets traffic-class ' + temp_list[temp_list.index('traffic-class') + 1] + ' mode strict\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + ' lldp dcbx-ets-config-tlv on\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + ' lldp dcbx-ets-recomm-tlv on\n'
                    line_str = line_str + NVUE + 'interface ' + interface_name + ' qos egress-scheduler profile default-ets\n'
                    
                elif (line_str.startswith('interface ethernet ') and line_str.find('ospf area') != -1):
                    line_str = line_str.replace('interface ethernet', NVUE +'interface')
                    line_str = line_str.replace(' ip', ' router')
                else:
                    line_str = '# MANUAL REVIEW ' + line_str
            ## VLAN Interface
            elif line_str.find('interface vlan') != -1:
                # DHCP
                if (line_str.find('dhcp relay') != -1):
                    line_str = line_str.replace('interface vlan ', NVUE +'service dhcp-relay default interface vlan')
                    line_str = line_str.replace('ip dhcp relay instance 1 downstream', '')
                # MAGP
                elif line_str.find('magp') != -1:
                    temp_list = line_str.split()
                    if line_str.find('virtual-router mac-address') != -1:
                        line_str = NVUE + 'interface vlan' + temp_list[temp_list.index('vlan') + 1] + ' ip vrr mac-address ' + temp_list[-1] + '\n'
                    elif line_str.find('virtual-router address ') != -1:
                        line_str = NVUE + 'interface vlan' + temp_list[temp_list.index('vlan') + 1] + ' ip vrr address ' + temp_list[-1] + '\n'
                        line_str = line_str + NVUE + 'interface vlan' + temp_list[temp_list.index('vlan') + 1] + ' ip vrr state up\n'
                    else:
                        line_str = '# FUTURE SUPPORT ' + line_str
                elif (line_str.find('vrrp') != -1 and line_str.find('address') != -1):
                    #line_str = '# MANUAL REVIEW ' + line_str
                    temp_list = line_str.split()
                    line_str = line_str.replace('interface vlan ', NVUE +'interface vlan')
                    line_str = re.sub('vrrp [0-9]+','ip vrr',line_str)
                    line_str = line_str.replace('\n', '')
                    line_str = line_str + '/24\n'
                    line_str = line_str + \
                        NVUE + 'interface vlan' + temp_list[2] + ' ip vrr mac-address auto\n' + \
                        NVUE + 'interface vlan' + temp_list[2] + ' ip vrr state up\n'
                elif line_str.find('vrrp') != -1:
                    line_str = '# FUTURE SUPPORT ' + line_str
                else:
                    line_str = '# MANUAL REVIEW ' + line_str

            ### Network interface configuration
            elif line_str.find('interface mgmt0') != -1:
                line_str = '# SCRIPT UNSUPPORTED ' + line_str

            # MLAG
            elif (line_str.startswith('interface vlan') or line_str.startswith('interface port-channel')) and line_str.find('ipl')!= -1:
                    line_str = '# MLAG CONVERSION ' + line_str

            # Catch all the other statements
            else:
               line_str = '# MANUAL REVIEW ' + line_str

        ### ROCE - Added in 1.4
        elif line_str.startswith('roce'):
            if line_str.find('semi-lossless') == -1:
                line_str = NVUE + 'qos roce enable on\n' + NVUE + 'qos roce mode ' + temp_list[-1] + '\n'
            else:
                line_str = '# MANUAL REVIEW ' + NVUE + 'qos roce enable on\n' + line_str
    
        ### VLAN Configuration
        elif line_str.startswith('vlan'):
            if line_str.find('name') != -1:
                description_string = ' '.join(temp_list[temp_list.index('name') + 1:]).replace('-','_').replace(':','_') 
                line_str = NVUE + 'interface vlan' + temp_list[1] + ' description ' + description_string + '\n'
            # Check for IP IGMP Configuration - added in 1.7
            elif line_str.find('ip igmp snooping') != -1:
                if line_str.find('snooping querier') != -1:
                    line_str = NVUE + 'bridge domain br_default multicast snooping enable on\n'
                    line_str = line_str + NVUE + 'bridge domain br_default multicast snooping querier enable on\n'
            else:
                line_str = line_str.replace('vlan ', NVUE +'bridge domain br_default vlan ')

        ### LAG Configuration
        elif line_str.startswith('lacp'):
            line_str = '# UNREQUIRED ' + line_str
        elif line_str.startswith('port-channel load-balance'):
            line_str = '# UNREQUIRED ' + line_str

        ### LLDP - Added in 1.4
        elif line_str.startswith('lldp tx-delay'):
            line_str = NVUE + 'service lldp tx-interval ' + temp_list[-1] + '\n'
        elif line_str.startswith('lldp tx-hold-multiplier'):
            line_str = NVUE + 'service lldp tx-hold-multiplier ' + temp_list[-1] + '\n'

        
        ### STP - Added in 1.4
        elif line_str.startswith('spanning-tree priority'):
            line_str = NVUE + 'bridge domain br_default stp priority ' + temp_list[-1] + '\n'
        elif line_str.startswith('spanning-tree port type'):
            if line_str.find('edge') != -1:
                line_str = '# Cumulus Linux enables PortAutoEdge by default\n'
        elif line_str.startswith('no spanning-tree'):
            line_str = line_str.replace('no spanning-tree', NVUE +'bridge domain br_default stp state down')

        ### MLAG configurations
        elif line_str.startswith('mlag-vip'):
            # Add the generic MLAG configs required for NVUE - Updated in 1.6
            line_str = '# UNREQUIRED ' + line_str
            line_str = line_str + NVUE + 'mlag enable on\n'
            line_str = line_str + NVUE + 'mlag peer-ip linklocal\n'
            line_str = line_str + '\n# MODIFY the IP address to match the mgmt IP of the peer switch in the MLAG pair\n' + NVUE + 'mlag backup 127.0.0.1 vrf mgmt\n'
            line_str = line_str + '\n# MODIFY the MAC address to match the physical MAC address of the switch\n' + NVUE + 'mlag mac-address 44:38:39:ff:00:00\n'
            line_str = line_str + NVUE + 'interface peerlink bond member ' + str(MLAGMEMBERINTERFACE) + '\n'
        
        ### Traffic pools
        elif line_str.startswith('traffic pool roce type lossless'):
            line_str = line_str.replace('traffic pool roce type lossless', NVUE +'qos roce mode lossless')
            line_str = '# MANUAL REVIEW ' + line_str
        elif (line_str.startswith('traffic pool roce') and (line_str.find('memory') != -1 or line_str.find('map') != -1)):
            line_str = '# SCRIPT UNSUPPORTED ' + line_str
        elif (line_str.startswith('traffic pool')) and (line_str.find('type lossless') != -1):
            line_str = '# MANUAL REVIEW ' + NVUE + 'qos traffic-pool default-lossless memory-percent 80\n'
            line_str = line_str + '# MANUAL REVIEW ' + NVUE + 'qos traffic-pool default-lossy memory-percent 20\n'


        ### L3 configuration
        elif line_str.startswith('ip routing vrf default') or line_str.startswith('ipv6 routing vrf default'):
            line_str = '# UNREQUIRED ' + line_str

        ### DCB
        #### PFC configuration
        elif line_str.startswith('dcb priority-flow-control'):
            if line_str.find('dcb priority-flow-control enable') != -1:
                line_str = NVUE + 'qos pfc default-global tx enable\n'
                line_str = line_str + NVUE + 'qos pfc default-global rx enable\n'
                line_str = line_str + NVUE + 'qos pfc default-global cable-length 50\n'
            elif line_str.find('dcb priority-flow-control priority') != -1:
                line_str = NVUE + 'qos pfc default-global switch-priority' + temp_list[temp_list.index('priority') + 1] + '\n'
            else:
                line_str = '# SCRIPT UNSUPPORTED ' + line_str
        elif line_str.startswith('interface port-channel'):
            if line_str.find('dcb priority-flow-control') != -1:
                line_str = '# SCRIPT UNSUPPORTED ' + line_str
        #### Application TLV - added in CL 5.9
        elif line_str.startswith('dcb application-priority'):
            if line_str.lower().find('iscsi') != -1:
                line_str = NVUE + 'service lldp application-tlv app iSCSI priority ' + temp_list[-1] + '\n'
                line_str = line_str + '# MANUAL REVIEW ' + NVUE + 'interface <interface_name/range> lldp application-tlv app iSCSI\n'

        ### Other IP configuration
        elif line_str.startswith('ip name-server vrf mgmt'):
            line_str = line_str.replace('ip name-server vrf mgmt', NVUE + 'service dns default server')
        elif line_str.startswith('ip name-server'):
            line_str = line_str.replace('ip name-server vrf vrf-default', NVUE + 'service dns default server')
        elif line_str.startswith('hostname'):
            line_str = line_str.replace('hostname ', NVUE +'system hostname ')

        ### Logging configuration
        elif line_str.startswith('logging'):
            if line_str.find('vrf') == -1:
                vrf_name = 'default'
            else:
                vrf_name = temp_list[temp_list.index('vrf') + 1]
                line_str = line_str.replace('vrf ' + vrf_name, '')
            if line_str.find(' port ') != -1 or line_str.find(' protocol ') != -1 or len(line_str.split()) == 2:
                line_str = line_str.replace('logging ', NVUE + 'service syslog default server ' )
            else:
                line_str = "# UNSUPPORTED " + line_str
            

        ### Network management configuration - Updated in 1.4
        elif line_str.startswith('clock timezone '):
            line_str = line_str.replace('clock', NVUE + 'system' )
            if line_str.find('UTC-offset') != -1: # Updated in 1.6 to account for timezone specified as an offset of UTC
                line_str = line_str.replace("UTC-offset UTC","Etc/GMT")
        elif line_str.startswith('ntp server'):
            temp_list = line_str.split()
            if line_str.find('keyID') != -1:
                line_str = '# FUTURE SUPPORT ' + line_str
            elif line_str.find('version') != -1:
                line_str = '# FUTURE SUPPORT ' + line_str
                line_str = line_str + '\n' + NVUE +'service ntp default server ' + temp_list[2] + '\n'
        elif line_str.find('ntp') != -1:
            line_str = '# MANUAL REVIEW ' + line_str


        ### SSH and Key configuration
        elif line_str.startswith('ssh client'):
            line_str = '# FUTURE SUPPORT ' + line_str


        ### Persistent prefix mode setting
        elif line_str.startswith('cli default prefix-modes enable'):
            line_str = '# UNREQUIRED ' + line_str

        elif line_str.startswith('vrf definition mgmt'):
            line_str = '# UNREQUIRED ' + line_str
        elif line_str.find('no switchport force') != -1: #look for changing onyx switchport to router port
            line_str = ''
        elif line_str.find('ipv6') != -1:
            line_str = '# FUTURE SUPPORT ' + line_str

        ### OSPF
        elif line_str.startswith('router ospf'):
            if line_str.find('vrf') != -1 :
                line_str = NVUE + 'vrf default router ospf\n'
            elif line_str.find('router-id') != -1:
                line_str = NVUE + 'vrf default router ospf router-id ' + temp_list[-1] + '\n'
            elif line_str.find('redistribute') != -1:
                if line_str.find('bgp') != -1:
                    line_str = NVUE + 'vrf default router ospf redistribute bgp enable on\n'
                elif line_str.find('static') != -1:
                    line_str = NVUE + 'vrf default router ospf redistribute static enable on\n'
                elif line_str.find('direct') != -1:
                    line_str = NVUE + 'vrf default router ospf redistribute connected enable on\n'
            elif line_str.find('timers') != -1:
                line_str = NVUE + 'vrf default router ospf timers spf delay ' + temp_list[-2] + '\n'
                line_str = line_str + NVUE + 'vrf default router ospf timers spf holdtime ' + temp_list[-1] + '\n'
            elif line_str.find('area') != -1:
                if line_str.find('default-cost') != -1:
                    line_str = NVUE + 'vrf default router ospf area ' + temp_list[3] + ' default-lsa-cost ' + temp_list[-1] + '\n'
                elif line_str.find('range') != -1:
                    line_str = NVUE + 'vrf default router ospf area ' + temp_list[3] + ' range ' + temp_list[5] + temp_list[6]
                    if line_str.find('not-advertise') != -1:
                        line_str = line_str + ' suppress on\n'
                    else:
                        line_str = '\n'
                elif line_str.find('stub') != -1:
                    line_str = NVUE + 'vrf default router ospf area ' + temp_list[3] + 'type stub\n'
            elif line_str.find('default-information') != -1:
                new_line = NVUE + 'vrf default router ospf default-originate enable on\n'
                if line_str.find('always') != -1:
                    new_line = new_line + NVUE + 'vrf default router ospf default-originate always on\n'
                if line_str.find('metric-type') != -1:
                    new_line = new_line + NVUE + 'vrf default router ospf default-originate metric-type' + temp_list[-1] + '\n'
                if line_str.find('metric') != -1 and line_str.find('metric-type') == -1:
                    new_line = new_line + NVUE + 'vrf default router ospf default-originate metric' + temp_list[temp_list.index('metric') + 1] + '\n'
                line_str = new_line   
            else:
                line_str = '# MANUAL REVIEW ' + line_str     
        
        ### BGP
        elif line_str.startswith('router bgp'):
            if line_str.find('vrf') != -1:
                vrf_name = temp_list[temp_list.index('vrf') + 1]
            else:
                vrf_name = 'default'
            if len(line_str.split()) == 3:
                line_str = NVUE + 'vrf ' + vrf_name + ' router bgp autonomous-system ' + temp_list[-1] + '\n'
            elif (len(line_str.split()) == 5):
                line_str = NVUE + 'vrf ' + vrf_name + ' router bgp autonomous-system ' + temp_list[2] + '\n'
            elif line_str.find('router-id') != -1:
                line_str = NVUE + 'vrf ' + vrf_name + ' router bgp router-id ' + temp_list[temp_list.index('router-id') + 1] + '\n'
            elif line_str.find('as-path multipath-relax') != -1:
                line_str = NVUE + 'vrf ' + vrf_name + ' router bgp path-selection multipath aspath-ignore on\n'
            elif line_str.find('redistribute') != -1:
                line_str = NVUE + 'vrf ' + vrf_name + ' router bgp address-family ipv4-unicast redistribute ' + temp_list[-1] + '\n'
            elif line_str.find('network') != -1:
                line_str = NVUE + 'vrf ' + vrf_name + ' router bgp address-family ipv4-unicast network ' + temp_list[-2] + temp_list[-1] + '\n'
            # Neighbor
            ## router bgp 64610 vrf default neighbor interface ethernet swp54 peer-group EVPN
            elif line_str.find('neighbor') != -1:
                if line_str.find('interface ethernet') != -1:
                    neighbor = temp_list[temp_list.index('neighbor') + 3]
                else:
                    neighbor = temp_list[temp_list.index('neighbor') + 1]
                
                if temp_list[-1] == 'peer-group':
                    # router bgp 64610 vrf default neighbor EVPN peer-group
                    BGPPEER.append(neighbor)

                if neighbor not in BGPPEER:
                    type = 'neighbor'
                else:
                    type = 'peer-group'

                if line_str.find('remote-as') != -1:
                    line_str = NVUE + 'vrf ' + vrf_name + ' router bgp ' + type + ' ' + neighbor + ' remote-as ' + temp_list[-1] + '\n'
                elif line_str.find('peer-group') != -1 and temp_list[-2] == 'peer-group':
                    line_str = NVUE + 'vrf ' + vrf_name + ' router bgp ' + type + ' ' + neighbor + ' peer-group ' + temp_list[-1] + '\n'
                elif line_str.find('peer-group') != -1 and temp_list[-1] == 'peer-group':
                    line_str = NVUE + 'vrf ' + vrf_name + ' router bgp ' + type + ' ' + neighbor + '\n'
                elif line_str.find('timers') != -1:
                    line_str = NVUE + 'vrf ' + vrf_name + ' router bgp ' + type + ' ' + neighbor + ' timers keepalive ' + temp_list[-2] + '\n'
                    line_str = line_str +  NVUE + 'vrf ' + vrf_name + ' router bgp ' + type + ' ' + neighbor + ' timers hold ' + temp_list[-1] + '\n'
                elif line_str.find('advertisement-interval') != -1:
                    line_str = NVUE + 'vrf ' + vrf_name + ' router bgp ' + type + ' ' + neighbor + ' timers route-advertisement ' + temp_list[-1] + '\n'
                elif line_str.find('address-family') != -1:
                    if line_str.find('ipv4-unicast') != -1:
                        family = 'ipv4-unicast'
                    elif line_str.find('ipv6-unicast') != -1:
                        family = 'ipv6-unicast'
                    elif line_str.find('l2vpn-evpn') != -1:
                        family = 'l2vpn-evpn' 
                    if line_str.find('activate') != -1:
                        line_str = NVUE + 'vrf ' + vrf_name + ' router bgp ' + type + ' ' + neighbor + ' address-family ' + family + ' enable on\n'
                    else:
                        line_str = "# MANUAL REVIEW " + line_str
                elif line_str.find('maximum-prefix') != -1:
                    line_str = '# FUTURE SUPPORT ' + line_str

                else:
                    line_str = "# MANUAL REVIEW " + line_str
            
            else:
                line_str = '# MANUAL REVIEW ' + line_str
        
        ### DHCP
        elif line_str.startswith('ip dhcp relay '):
            if line_str.find ('address vrf') != -1:
                temp_list = line_str.split()
                line_str = NVUE + 'server dhcp-relay ' + temp_list[-2] + ' server ' + temp_list[-1] + '\n'
            else:
                line_str = line_str.replace('ip dhcp relay instance 1 address ', NVUE +'service dhcp-relay default server ')
                line_str = line_str.replace('ip dhcp relay instance 1 downstream', '')
        elif (line_str.startswith('ip dhcp relay instance') and line_str.find('vrf default') != -1):
            line_str = '# UNREQUIRED ' + line_str


        ### Static Routing
        elif line_str.startswith('ip route'):
            temp_list = line_str.split()
            if temp_list[-2].find('/') != -1:
                line_str = NVUE + 'vrf default router static ' + temp_list[-2] + ' via ' + temp_list[-1] + '\n'
            else:
                line_str = NVUE + 'vrf default router static ' + temp_list[-3] + '/' + temp_list[-2] + ' via ' + temp_list[-1] + '\n'

        ### SNMP - Added in 1.4
        elif line_str.startswith('snmp-server'):    
            temp_list = line_str.split()
            if line_str.startswith('snmp-server enable') or line_str.startswith('snmp-server vrf ' + temp_list[2] + ' enable'):
                line_str = NVUE + 'service snmp-server enable on\n'
                line_str = line_str + NVUE + 'service snmp-server listening-address localhost\n'
            elif line_str.find('contact') != -1:
                line_str = NVUE + 'service snmp-server system-contact ' + temp_list[-1] + '\n'
            elif line_str.find('community') != -1 and line_str.find('ro') != -1:
                line_str = NVUE + 'service snmp-server readonly-community ' + temp_list[-2] + ' access any\n'
            else:
                line_str = '# MANUAL REVIEW ' + line_str

        ### route-map - Added in 1.10
        elif line_str.startswith('route-map'):   
            # route-map CUSTOMER-IN permit 1 match ip address DEFAULT-ROUTE
            # route-map DENY-ALL deny 10 
            temp_list = line_str.split()
            base_rm = NVUE + 'router policy route-map ' + temp_list[1] + " rule " + temp_list[3]
            if line_str.find('deny') != -1 and len(temp_list) == 4:
                line_str = base_rm + " action deny\n"
            elif line_str.find('match ip address') != -1 or line_str.find('match ipv6 address') != -1:
                line_str = base_rm + ' match ip-prefix-list ' + temp_list[temp_list.index('address') + 1] + '\n'
                if line_str.find('ipv6') != -1:
                    ip_type = 'ipv6'
                else:
                    ip_type = 'ipv4'
                line_str = line_str + base_rm + ' match type ' + ip_type + '\n'
                line_str = line_str + base_rm + " action " + temp_list[2] + '\n'
            elif line_str.find('ip next-hop') != -1: # Can be match or set
                line_str = base_rm + ' ' + temp_list[4] + ' ip-nexthop ' + temp_list[temp_list.index('next-hop') + 1] + '\n'
                line_str = line_str + base_rm + " action " + temp_list[2] + '\n'
            elif line_str.find('metric') != -1: # Can be match or set
                line_str = base_rm + ' ' + temp_list[4] + ' metric ' + temp_list[temp_list.index('metric') + 1] + '\n'
                line_str = line_str + base_rm + " action " + temp_list[2] + '\n'
            elif line_str.find('match as-path') != -1:
                line_str = base_rm + ' match as-path-list ' + temp_list[temp_list.index('as-path') + 1] + '\n'
                line_str = line_str + base_rm + " action " + temp_list[2] + '\n'
            elif line_str.find('match community') != -1:
                line_str = base_rm + ' match community-list ' + temp_list[temp_list.index('community') + 1] + '\n'
                line_str = line_str + base_rm + " action " + temp_list[2] + '\n'
            elif line_str.find('set local-preference') != -1:
                line_str = base_rm + ' set local-preference ' + temp_list[temp_list.index('local-preference') + 1] + '\n'
                line_str = line_str + base_rm + " action " + temp_list[2] + '\n'
            elif line_str.find('set origin') != -1:
                line_str = base_rm + ' set origin ' + temp_list[temp_list.index('origin') + 1] + '\n'
                line_str = line_str + base_rm + " action " + temp_list[2] + '\n'
            elif line_str.find('set weight') != -1:
                line_str = base_rm + ' set weight ' + temp_list[temp_list.index('weight') + 1] + '\n'
                line_str = line_str + base_rm + " action " + temp_list[2] + '\n'
            else:
                line_str = '# MANUAL REVIEW ' + line_str
        
        ### prefix lists - Added in 1.10
        elif line_str.startswith('ip prefix-list'):   
            # ip prefix-list CUSTOMER-CIDR
            # ip prefix-list CUSTOMER-CIDR bulk-mode
            # ip prefix-list CUSTOMER-CIDR seq 10 permit 10.100.111.0 /24 eq 24
            # ip prefix-list CUSTOMER-CIDR commit
            temp_list = line_str.split()
            if line_str.find('ipv6') != -1:
                ip_type = 'ipv6'
            else:
                ip_type = 'ipv4'
            
            if line_str.find('seq') != -1:   
                base_rm = NVUE + 'router policy prefix-list ' + temp_list[temp_list.index('prefix-list') + 1] + " rule " + temp_list[temp_list.index('seq') + 1]
                if line_str.find('permit') != -1:
                    action = 'permit'
                else: 
                    action = 'deny'
                if 'eq' in temp_list:
                    line_str = base_rm + ' match ' + temp_list[temp_list.index(action) + 1] + temp_list[temp_list.index(action) + 2] + ' min-prefix-len ' + temp_list[temp_list.index('eq') + 1] + '\n'
                    line_str = line_str + base_rm + ' match ' + temp_list[temp_list.index(action) + 1] + temp_list[temp_list.index(action) + 2] + ' max-prefix-len ' + temp_list[temp_list.index('eq') + 1] + '\n'
                if 'le' in temp_list:
                    line_str = base_rm + ' match ' + temp_list[temp_list.index(action) + 1] + temp_list[temp_list.index(action) + 2] + ' max-prefix-len ' + temp_list[temp_list.index('le') + 1] + '\n'
                if 'ge' in temp_list:
                    line_str = base_rm + ' match ' + temp_list[temp_list.index(action) + 1] + temp_list[temp_list.index(action) + 2] + ' min-prefix-len ' + temp_list[temp_list.index('ge') + 1] + '\n'
                line_str = line_str + base_rm + " action " + action + '\n'
                line_str = line_str + NVUE + 'router policy prefix-list ' + temp_list[temp_list.index('prefix-list') + 1] + ' type ' + ip_type + '\n'
            else:
                line_str = '# MANUAL REVIEW ' + line_str

        ### access-list - Added in 1.10
        elif line_str.startswith('ipv4 access-list') or line_str.startswith('ipv6 access-list') or line_str.startswith('mac access-list'):   
            # ipv4 access-list TE-EDGE-INBOUND4
            # ipv4 access-list TE-EDGE-INBOUND4 seq-number 10 remark DNS Traffic 
            # ipv4 access-list TE-EDGE-INBOUND4 seq-number 20 permit udp 10.100.111.7 mask 255.255.255.255 any eq-destination 53 
            # ipv4 access-list TE-EDGE-INBOUND4 seq-number 70 permit icmp any any 
            # ipv4 access-list TE-EDGE-INBOUND4 seq-number 90 permit udp any any dest-port-range 33434 33523 
            # ipv4 access-list TE-EDGE-INBOUND4 seq-number 130 permit ip any 10.100.111.0 mask 255.255.255.0 
            # ipv4 access-list TE-EDGE-INBOUND4 seq-number 150 permit udp any any eq-destination 123 
            # ipv4 access-list TE-EDGE-INBOUND4 seq-number 160 deny tcp any 10.100.111.2 mask 255.255.255.255 eq-destination 6789 
            # ipv4 access-list TE-EDGE-INBOUND4 seq-number 180 deny tcp any 10.100.111.2 mask 255.255.255.255 dest-port-range 6800 7300 
            # ipv4 access-list TE-EDGE-INBOUND4 seq-number 260 deny ip any any log 
            temp_list = line_str.split()
            acl_type = temp_list[0]
            if len(temp_list) > 3:
                action_type = temp_list[temp_list.index('seq-number') + 2]
                protocol_type = temp_list[temp_list.index(action_type) + 1]
                base_acl = NVUE + 'acl ' + temp_list[temp_list.index('access-list') + 1] + " rule " + temp_list[temp_list.index('seq-number') + 1]
                
                if line_str.startswith('ipv4 access-list') and (line_str.find('permit') != -1 or line_str.find('deny') !=-1 ): 
                    # Protocol type
                    if (line_str.find('icmp') != -1 or line_str.find('udp') != -1 or line_str.find('tcp') != -1) and line_str.find('remark') == -1:
                        line_str = base_acl + ' match ip protocol ' + protocol_type + '\n'
                    elif line_str.find('ip') != -1 :
                        line_str = ''
                    # TCP established
                    if 'tcp' in temp_list and 'established' in temp_list :
                        line_str = line_str + base_acl + ' match ip tcp state established\n'
                    # Check subnet masks and convert to CIDR
                    # if source is any
                    if temp_list[temp_list.index(protocol_type) + 1] == 'any':
                        source = 'any'
                        # if dest is any
                        if temp_list[temp_list.index(protocol_type) + 2] == 'any':
                            dest = 'any'
                        # if dest is IP address
                        else:
                            dest = temp_list[temp_list.index(protocol_type) + 2]
                            if temp_list[temp_list.index(protocol_type) + 3] == 'mask':
                                subnet_mask = IPv4Network('0.0.0.0/' + temp_list[temp_list.index('mask') + 1] ).prefixlen
                                dest = dest + '/' + str(subnet_mask)
                    # source is an IP
                    else:
                        source = temp_list[temp_list.index(protocol_type) + 1]
                        if temp_list[temp_list.index(protocol_type) + 2] == 'mask':
                            subnet_mask = IPv4Network('0.0.0.0/' + temp_list[temp_list.index('mask') + 1] ).prefixlen
                            source = source + '/' + str(subnet_mask)
                        # if dest is any
                        if temp_list[temp_list.index(protocol_type) + 4] == 'any':
                            dest = 'any'
                        # if dest is IP address
                        else:
                            dest = temp_list[temp_list.index(protocol_type) + 4]
                            if temp_list[temp_list.index(protocol_type) + 5] == 'mask':
                                subnet_mask = IPv4Network('0.0.0.0/' + temp_list[temp_list.index('mask') + 1] ).prefixlen
                                dest = dest + '/' + str(subnet_mask)
                    # source port
                    if 'src-port' in temp_list:
                        source_port = temp_list[temp_list.index('src-port') + 1]
                    elif 'eq-source' in temp_list:
                        source_port = temp_list[temp_list.index('eq-source') + 1]
                    elif 'src-port-range' in temp_list:
                        source_port = temp_list[temp_list.index('src-port-range') + 1] + '-' + temp_list[temp_list.index('src-port-range') + 2]
                    else:
                        source_port = 'any'
                    # dest port
                    if 'dest-port' in temp_list:
                        dest_port = temp_list[temp_list.index('dest-port') + 1]
                    elif 'eq-destination' in temp_list:
                        dest_port = temp_list[temp_list.index('eq-destination') + 1]
                    elif 'dest-port-range' in temp_list:
                        dest_port = temp_list[temp_list.index('dest-port-range') + 1] + '-' + temp_list[temp_list.index('dest-port-range') + 2]
                    else: 
                        dest_port = 'any'
                    line_str = line_str + base_acl + ' match ip source-ip ' + source + '\n'
                    line_str = line_str + base_acl + ' match ip dest-ip ' + dest + '\n'
                    line_str = line_str + base_acl + ' match ip source-port ' + source_port + '\n'
                    line_str = line_str + base_acl + ' match ip dest-port ' + dest_port + '\n'
                    
                else:
                    line_str = '# MANUAL REVIEW ' + line_str + '\n'
                # Action and ACL type
                # Log
                if line_str.find('log') != -1:
                    line_str = line_str + base_acl + " action log\n"
                # Comment
                if line_str.find('remark') != -1:
                    line_str = base_acl + ' remark "' + " ".join(temp_list[(temp_list.index('remark') + 1):]) + '"\n'
                else:
                    line_str = line_str + base_acl + ' action ' + action_type + '\n'
                line_str = line_str + base_acl + ' type ' + acl_type + '\n'
            else:
                line_str = "# UNREQUIRED " + line_str + '\n'
            

        ### TACACS - Added in 1.5, CL 5.4 - updated in 1.9 since TACAS fails in CL, unless you mention the host, better to manually review it and add it in
        elif line_str.startswith('tacacs-server'):    
            
            '''
            if line_str.find(' vrf ') != -1:
                line_str = NVUE + 'system aaa tacacs vrf ' + temp_list[temp_list.index('vrf') + 1] + '\n'
            else:
                line_str = ''

            if line_data.find('enable') != -1:
                line_str = line_str + NVUE + 'system aaa tacacs enable on\n'
                line_str = line_str + NVUE + 'system aaa authentication-order 5 tacacs\n'
            elif line_data.find('timeout') != -1:
                line_str = line_str + NVUE + 'system aaa tacacs timeout ' + temp_list[temp_list.index('timeout') + 1] + '\n'
            elif line_data.find('host') != -1:
                line_str = line_str + NVUE + 'system aaa tacacs server 1 host ' + temp_list[temp_list.index('host') + 1] + '\n'
                if line_data.find('auth-port') != -1:
                    line_str = line_str + NVUE + 'system aaa tacacs server 1 port ' + temp_list[temp_list.index('auth-port') + 1] + '\n'
                if line_data.find(' key ') != -1:
                    line_str = line_str + NVUE + 'system aaa tacacs server 1 secret ' + temp_list[temp_list.index('key') + 1] + '\n' 
            '''
            line_str = '# MANUAL REVIEW ' + line_str + '\n'
            
        ### Final catchall
        else:
            line_str = '# MANUAL REVIEW ' + line_str


    if DEBUG:
        print("CONVERT:\n" +line_str)
    if write_line:
       line_output.extend(line_str)
       #print (line_str)
    else:
       write_line = True

    if DEBUG:
        print('\n')

for i in line_output:                       # go thouw line by line of the input file
    output_file.write(i)                     # write new output file
    #print(i)                               # dump it on the screen too
input_file.close()                          # close the input file
output_file.close()                          # close the output file

print ('Convert Onyx to Cumulus Linux script - version '+current_ver)
print ('Using output type',NVUE)
print ('Output file is "', outputfile)
